/* eslint-disable no-undef */
import { Product } from '../../src/models/product.js';
import { Gender } from '../../src/models/gender.js';
import { Cream } from '../../src/models/cream.js';
import { Scent } from '../../src/models/scent.js';

describe('Cream', () => {


  describe('name', () => {

    it('constructor should throw if no name was passed or the passed value is empty string', () => {
      expect(() => new Product(undefined, 'Test brand', 10, Gender.Men)).toThrow();
    });

    it('constructor should throw if name is too short', () => {
      expect(() => new Product('aa', 'Test brand', 10, Gender.Women)).toThrow();
    });

    it('constructor should throw if name is too long', () => {
      expect(() => new Product('a'.repeat(16), 'Test brand', 10, Gender.Women)).toThrow();
    });

    it('constructor should set name with the correct value', () => {
      const product = new Product('Test name', 'Test brand', 10, Gender.Unisex);

      expect(product.name).toBe('Test name');
    });

    it('should not be able to set the value of name from outside the class', () => {
      const product = new Product('Test name', 'Test brand', 10, Gender.Unisex);

      expect(() => product.name = 'New name').toThrow();
    });

  });

  describe('brand', () => {

    it('constructor should throw if no brand was passed or the passed value is empty string', () => {
      expect(() => new Cream('Test name', undefined, 10, Gender.Men, Scent.Rose)).toThrow();
    });

    it('constructor should throw if brand is too short', () => {
      expect(() => new Cream('Test name', 'a', 10, Gender.Women, Scent.Rose)).toThrow();
    });

    it('constructor should throw if brand is too long', () => {
      expect(() => new Cream('Test name', 'a'.repeat(16), 10, Gender.Women, Scent.Rose)).toThrow();
    });

    it('constructor should set brand with the correct value', () => {
      const cream = new Cream('Test name', 'Test brand', 10, Gender.Unisex, Scent.Rose);

      expect(cream.brand).toBe('Test brand');
    });

    it('should not be able to set the value of brand from outside the class', () => {
      const cream = new Cream('Test name', 'Test brand', 10, Gender.Unisex, Scent.Rose);

      expect(() => cream.brand = 'New brand').toThrow();
    });

  });

  describe('class', () => {

    it('should extend Product', () => {
      expect(Cream.prototype).toBeInstanceOf(Product);
    });

  });

  describe('scent', () => {

    it('should throw if the value of scent is not valid', () => {
      expect(() => new Cream('Test name', 'Test brand', 10, Gender.Unisex, 'InvalidKeyValue')).toThrow();
    });

    it('should set scent to the correct value', () => {
      const cream = new Cream('Test name', 'Test brand', 10, Gender.Unisex, Scent.Rose);

      expect(cream.scent).toBe(Scent.Rose);
    });

    it('should not be able to set the value of scent from outside the class', () => {
      const cream = new Cream('Test name', 'Test brand', 10, Gender.Unisex, Scent.Levander);

      expect(() => cream.scent = Scent.Levander).toThrow();
    });

  });

  describe('print', () => {

    it('should override Product\'s additionalInfo method', () => {
      expect(Cream.prototype.additionalInfo).toBeInstanceOf(Function);
    });

    it('should return the correct string', () => {
      const cream = new Cream('Test name', 'Test brand', 10, Gender.Unisex, Scent.Rose);

      const result = cream.print();

      expect(result).toContain('Scent: Rose');
    });

    it('should call additionalInfo once', () => {
      const cream = new Cream('Test name', 'Test brand', 10, Gender.Unisex, Scent.Levander);

      jest.spyOn(cream, 'additionalInfo');

      // eslint-disable-next-line no-unused-vars
      const result = cream.print();

      expect(cream.additionalInfo).toHaveBeenCalledTimes(1);
    });
  });
});
