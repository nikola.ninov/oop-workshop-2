import { Product } from './product.js';
import { Scent } from './scent.js';

export class Cream extends Product {
  #scent;
  static #minNameLength = 3;
  static #maxNameLength = 15;
  static #minBrandLength = 3;
  static #maxBrandLength = 15;

  constructor(name, brand, price, gender, scent) {
    super(name, brand, price, gender);
    this.#scent = scent;
    this.validateScent(scent);
    this.validateBrand(brand.length);
    this.validateName(name.length);
  }
  get scent() {
    return this.#scent;
  }

  validateScent(value) {
    if (!Object.values(Scent).includes(value)) {
      throw new Error('Invalid scent type.');
    }
  }

  validateName(value) {
    if (value < Cream.#minNameLength || value > Cream.#maxNameLength) {
      throw new Error('Name should be between 3 and 15 characters long.');
    }
  }

  validateBrand(value) {
    if (!this.brand) {
      throw new Error('Invalid brand!');
    }
    if (value < Cream.#minBrandLength || value > Cream.#maxBrandLength) {
      throw new Error('Name should be between 3 and 15 characters long.');
    }
  }

  /**
  Gets additional information about the Cream.
  @return {string} Additional information about the Cream.
  */
  additionalInfo() {
    return `Scent: ${this.scent}`;
  }
}
