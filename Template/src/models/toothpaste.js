import { Product } from './product.js';
// eslint-disable-next-line no-unused-vars
import { Gender } from './gender.js';

export class Toothpaste extends Product {

  #ingredients;
  static INGREDIENTS_ZERO_CHECK;

  /**
   * Creates a new instance of Toothpaste.
   * @param {string} name - The name of the toothpaste.
   * @param {string} brand - The brand of the toothpaste.
   * @param {number} price - The price of the toothpaste.
   * @param {Gender} gender - The gender for which the toothpaste is suitable.
   * @param {string} ingredients - The ingredients used in the toothpaste.
   */
  constructor(name, brand, price, gender, ingredients) {
    super(name, brand, price, gender);
    this.#ingredients = ingredients;
    this.validateIngredients();
  }

  /**
   * Gets the ingredients used in the toothpaste.
   * @return {string} The ingredients of the toothpaste.
   */
  get ingredients() {
    return this.#ingredients;
  }

  /**
   * Validates that the ingredients of the toothpaste are not empty.
   * @throws {Error} If the ingredients are empty.
   */
  validateIngredients() {
    if (!this.#ingredients || this.#ingredients.trim().length === Toothpaste.INGREDIENTS_ZERO_CHECK) {
      throw new Error('Ingredients should not be empty.');
    }
  }

  /**
   * Gets additional information about the toothpaste.
   * @return {string} Additional information about the toothpaste.
   */
  additionalInfo() {
    return `Ingredients: ${this.#ingredients}`;
  }
}
