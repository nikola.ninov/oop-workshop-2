import { Product } from './product.js';
import { Usage } from './usage.js';
// import { Gender } from './gender.js';

export class Shampoo extends Product {
  /**
   * Represents a shampoo product.
   * @extends Product
   */

  #milliliters;
  #usage;
  static MILI_POSITIVE_CHECK=0;

  /**
   * Creates a new instance of Shampoo.
   * @param {string} name - The name of the shampoo.
   * @param {string} brand - The brand of the shampoo.
   * @param {number} price - The price of the shampoo.
   * @param {Gender} gender - The gender for which the shampoo is suitable.
   * @param {number} milliliters - The volume of the shampoo in milliliters.
   * @param {Usage} usage - The intended usage of the shampoo (EveryDay or Medical).
   */
  constructor(name, brand, price, gender, milliliters, usage) {
    super(name, brand, price, gender);
    this.#milliliters = milliliters;
    this.#usage = usage;
    this.validateMilliliters();
    this.validateUsage();
  }

  /**
   * Gets the volume of the shampoo in milliliters.
   * @return {number} The volume of the shampoo.
   */
  get milliliters() {
    return this.#milliliters;
  }

  /**
   * Gets the intended usage of the shampoo.
   * @return {Usage} The usage type of the shampoo (EveryDay or Medical).
   */
  get usage() {
    return this.#usage;
  }

  /**
   * Validates that the volume of the shampoo is not negative.
   * @throws {Error} If the volume is negative.
   */
  validateMilliliters() {
    if (this.#milliliters < Shampoo.MILI_POSITIVE_CHECK) {
      throw new Error('Milliliters cannot be negative.');
    }
  }

  /**
   * Validates that the usage type of the shampoo is valid.
   * @throws {Error} If the usage type is not EveryDay or Medical.
   */
  validateUsage() {
    if (!Object.values(Usage).includes(this.#usage)) {
      throw new Error('Invalid usage type.');
    }
  }

  /**
    Gets additional information about the shampoo.
    @return {string} Additional information about the shampoo.
  */
  additionalInfo() {
    return `Milliliters: ${this.#milliliters}\nUsage: ${this.#usage}`;
  }
}
