import { Gender } from './gender.js';

export class Product {

  static #minNameLength = 3;
  static #maxNameLength = 10;
  static #minBrandLength = 2;
  static #maxBrandLength = 10;
  static #minPrice = 0;
  #name;
  #brand;
  #price;
  #gender;

  /**
  * @param {string} name
  * @param {string} brand
  * @param {number} price
  * @param {Gender} gender
  */
  constructor(name, brand, price, gender) {
    this.#name = name;
    this.#brand = brand;
    this.#price = price;
    this.#gender = gender;
    this.validateBrand();
    this.validateName();
    this.validateGender();
    this.validatePrice();
  }

  /**
  * @type {string}
  */
  get name() {
    return this.#name;
  }

  /**
  * @type {string}
  */
  get brand() {
    return this.#brand;
  }

  /**
  * @type {number}
  */
  get price() {
    return this.#price;
  }

  /**
  * @type {Gender}
  */
  get gender() {
    return this.#gender;
  }

  validateName() {
    if (!this.name) {
      throw new Error('Invalid name!');
    }
    if (this.name.length < Product.#minNameLength || this.name.length > Product.#maxNameLength) {
      throw new Error(`Product name length must be between ${Product.#minNameLength} and ${Product.#maxNameLength}`);
    }
  }

  validateBrand() {
    if (!this.brand) {
      throw new Error('Invalid brand!');
    }

    if (this.brand.length < Product.#minBrandLength || this.brand.length > Product.#maxBrandLength) {
      throw new Error(`Product brand length must be between ${Product.#minBrandLength} and ${Product.#maxBrandLength}`);
    }
  }
  validateGender() {
    if (!Object.keys(Gender).some(key => Gender[key] === this.gender)) { // if (!Gender.hasOwnProperty(gender)) {
      throw new Error(`Invalid gender type value ${this.gender}!`);
    }

  }
  validatePrice() {
    if (this.price < Product.#minPrice) {
      throw new Error(`Product price must be greater than ${this.minPrice}`);
    }
  }

  /**
  * @return {string}
  */

  print() {
    const productInfo = [
      `#${this.#name} ${this.#brand}`,
      ` #Price: $${this.#price}`,
      ` #Gender: ${Gender[this.#gender]}`,
      `${this.additionalInfo()}`,
      ' ===',
    ];

    return productInfo.join('\r\n');
  }


  /**
  * @abstract
  *
  * @return {string}
  */
  additionalInfo() {
    throw new Error('Not implemented! Implementation must be provided by the classes extending Product!');
  };
}
